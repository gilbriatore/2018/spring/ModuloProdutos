package br.edu.up.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.up.app.dominio.Produto;
import br.edu.up.app.repository.ProdutoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppProdutosTests {
	
	@Autowired
	ProdutoRepository repository;

	@Test
	public void testarInclusaoDeProdutos() {
		
		Produto p = new Produto("Computador", 3000.00);
		repository.save(p);
		
	}

}
